<?php

namespace Drupal\system_monitor_demo\Plugin\SystemMonitorTask;

use Drupal\system_monitor\Plugin\SystemMonitorTask\SystemMonitorTaskBase;

/**
 * A test System Monitor Task.
 *
 * @SystemMonitorTask(
 *   id = "system_monitor_example_task_2",
 *   label = @Translation("System Monitor Example Task 2"),
 *   description = @Translation("This is an example task"),
 *   monitor_type = "system_monitor_examples_demo",
 *   tags = {
 *     "system_monitor_examples"
 *   }
 * )
 */
class SystemMonitorExampleTask2 extends SystemMonitorTaskBase {

  /**
   * The check to run for this System Monitor task.
   *
   * @return int
   *   The status value returned by the monitor task.
   */
  public function runTask() {
    \Drupal::messenger()->addStatus('Success');
    $this->createOrUpdateIncident('This is a successful task.', [], 6);
    return TRUE;
  }

}
