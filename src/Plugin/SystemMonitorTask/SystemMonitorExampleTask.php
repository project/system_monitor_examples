<?php

namespace Drupal\system_monitor_demo\Plugin\SystemMonitorTask;

use Drupal\system_monitor\Plugin\SystemMonitorTask\SystemMonitorTaskBase;

/**
 * A test System Monitor Task.
 *
 * @SystemMonitorTask(
 *   id = "system_monitor_example_task",
 *   label = @Translation("System Monitor Example Task"),
 *   description = @Translation("This is an example task"),
 *   monitor_type = "system_monitor_examples_demo",
 *   tags = {
 *     "system_monitor_examples"
 *   }
 * )
 */
class SystemMonitorExampleTask extends SystemMonitorTaskBase {

  /**
   * The check to run for this System Monitor task.
   *
   * @return int
   *   The status value returned by the monitor task.
   */
  public function runTask() {
    \Drupal::messenger()->addStatus('Success');
    $this->createOrUpdateIncident('This is a failed task.', 'Description of the faile event that occured.', 1);
    return TRUE;
  }
}
