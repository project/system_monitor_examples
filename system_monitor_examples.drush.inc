<?php

/**
 * @file
 * Contains the code to generate the custom drush commands for system_monitor.
 */

use Drupal\Core\Cache\Cache;
use Drupal\system_monitor\SystemMonitorLogLevel;

/**
 * Implements hook_drush_command().
 */
function system_monitor_examples_drush_command() {
  $items = [];
  $items['sme-gen-logs'] = [
    'description' => 'Generate test logs',
    'aliases' => ['sme:gl'],
  ];
  return $items;
}

/**
 * Generate demo log values for system monitor to test the status page.
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 */
function drush_system_monitor_examples_sme_gen_logs() {

  // Clear out the current system monitor database logs.
  \Drupal::database()->truncate('system_monitor_log')->execute();

  $system_monitor_utility = \Drupal::service('system_monitor.utility');
  $system_monitor_plugin_manager = \Drupal::service('plugin.manager.system_monitor_task');
  $system_monitor_logger = \Drupal::service('system_monitor_logger');

  $active_monitors = $system_monitor_utility->getActiveMonitorIds();
  $log_levels = $system_monitor_logger->getRfcToSystemMonitorMap();

  foreach ($active_monitors as $active_monitor) {
    $tasks = $system_monitor_plugin_manager->getAvailableTasksByMonitorId($active_monitor, [], TRUE);
    $first_task = TRUE;
    foreach ($tasks as $task => $task_label) {
      $num_log = 120;
      $logs = [];
      for ($days_ago = $num_log; $days_ago >= 0; $days_ago--) {
        $timestamp = strtotime("-{$days_ago} days") + rand(1, 10);
        // Always make the last entry for the first task be "unhealthy".
        if ($days_ago === 0 && $first_task) {
          $status = 1;
          $first_task = FALSE;
        }
        else {
          $status = (is_int($days_ago / 3) || is_int($days_ago / 4)) ? 6 : 1 + 3 * rand(0, 2);
        }
        $logs[$timestamp] = [
          'status' => $status,
          'values' => [
            'monitor_task_name' => $task,
            'monitor_type' => $active_monitor,
            'description' => 'Description of the event that occured for the ' . $task_label . ' task.',
            'timestamp' => $timestamp,
            'first_report' => 0,
          ],
        ];
      }
      ksort($logs);
      $ok_status = SystemMonitorLogLevel::STATUS_OK;
      $previous_status = $ok_status;
      foreach ($logs as $log) {
        $log_status = $log_levels[$log['status']];
        if ($log_status > $ok_status && $previous_status == $ok_status) {
          $log['values']['first_report'] = 1;
        }
        $previous_status = $log_status;
        $system_monitor_logger->log($log['status'], 'Test Message', $log['values']);
      }
    }
  }
  Cache::invalidateTags(['system_monitor', 'sm_status_page']);
}
