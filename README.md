## System Monitor: Examples
This module is a support module providing example monitors and tasks for the
System Monitor module.

This module provides examples that should be adapted into custom modules.

*It is not intended for production use.*

### New monitors and tasks
To submit an idea for inclusion in System Monitor: Examples, or if you just have
a great idea you wanted to share as a use case, create an issue in the queue.

https://www.drupal.org/project/issues/system_monitor_examples

### Requirements
System Monitor (https://www.drupal.org/projects/system_monitor)

### Contributors
* Melissa Bent (merauluka)
* Rob Powell (robpowell)

### Supporting Partners
*Mediacurrent* (https://www.mediacurrent.com)
Open source strategy, design, and development that grows your digital ROI
